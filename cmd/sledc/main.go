package main

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"time"

	"github.com/insomniacslk/dhcp/dhcpv4"
	"github.com/minio/minio-go/v7"
	"github.com/spf13/cobra"
	"github.com/vishvananda/netlink"

	"gitlab.com/mergetb/api/facility/v1/go"
	"gitlab.com/mergetb/tech/rtnl"
	"gitlab.com/mergetb/tech/shared/storage/decompression"

	"github.com/u-root/u-root/pkg/boot"
	"github.com/u-root/u-root/pkg/boot/kexec"
	"github.com/u-root/u-root/pkg/dhclient"
	"github.com/u-root/u-root/pkg/uio"
)

var (
	mac       string
	image     string
	retries   = 10
	benchmark = false
)

func main() {

	root := &cobra.Command{
		Use:   "sledc",
		Short: "The Sled imaging client",
	}

	run := &cobra.Command{
		Use:   "run",
		Short: "Run the Sled imaging client",
		Args:  cobra.NoArgs,
		Run:   func(*cobra.Command, []string) { runClient() },
	}
	run.Flags().BoolVarP(
		&benchmark, "benchmark", "b", benchmark, "continously run the image copying and decrypting, for benchmarking")
	root.AddCommand(run)

	var server string
	stamp := &cobra.Command{
		Use:   "stamp <image> <disk>",
		Short: "Stamp an image on the disk from a remote S3 server",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			err := stampImage(args[0], args[1], server)
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	stamp.Flags().StringVarP(
		&server, "server", "s", "images:9000", "S3 server to fetch image from")
	root.AddCommand(stamp)

	var cmdline string
	kexec := &cobra.Command{
		Use:   "kexec <kernel> <initramfs>",
		Short: "Fetch and kexec into a kernel/initramfs pair",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			err := kexecImagePrepare(args[0], args[1], server, cmdline)
			if err != nil {
				log.Fatal(err)
			}

			err = kexecImageReboot()
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	kexec.Flags().StringVarP(
		&server, "server", "s", "images:9000", "S3 server to fetch image from")
	kexec.Flags().StringVarP(&cmdline, "cmdline", "c", "", "Kernel command line args")
	root.AddCommand(kexec)

	root.Execute()

}

func runClient() {

	log.Printf("starting sled")

	{
		rtx, err := rtnl.OpenDefaultContext()
		if err != nil {
			log.Fatalf("rtnl open: %v", err)
		}
		defer rtx.Close()

		lnks, err := rtnl.ReadLinks(rtx, nil)
		if err != nil {
			log.Fatalf("rtnl read links: %v", err)
		}

		for _, l := range lnks {
			fmt.Printf("lnk %s: %+v", l.Info.Name, l)
		}
	}

	// preparation loop
	for {
		err := prepare()
		if err != nil {
			log.Printf("%+v", err)
			time.Sleep(1 * time.Second)
			continue
		}
		break
	}

	for {
		err := runsled()
		if err != nil {
			log.Printf("%+v", err)
			time.Sleep(1 * time.Second)
		}
	}

}

func findInfranetLink() (string, error) {

	// read the infranet kernel paramter supplied to us from Mars
	macstr, err := readKernelParam("inframac")
	if err != nil {
		return "", fmt.Errorf("read kernelparam inframac: %v", err)
	}

	mac, err := net.ParseMAC(macstr)
	if err != nil {
		return "", fmt.Errorf("parse MAC %s: %v", macstr, err)
	}

	rtx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return "", fmt.Errorf("rtnl open: %v", err)
	}
	defer rtx.Close()

	lnks, err := rtnl.ReadLinks(rtx, nil)
	if err != nil {
		return "", fmt.Errorf("rtnl read lniks: %v", err)
	}

	for _, l := range lnks {
		if bytes.Equal(mac, l.Info.Address) {
			return l.Info.Name, nil
		}
	}

	return "", fmt.Errorf("could not find link with MAC address %s", macstr)

}

func prepare() error {

	ifx, err := findInfranetLink()
	if err != nil {
		return fmt.Errorf("find infranet link: %v", err)
	}

	// bring the infranet network interface up
	lnk, err := dhclient.IfUp(ifx, time.Duration(10*time.Second))
	if err != nil {
		return fmt.Errorf("bring link %v up: %v", lnk, err)
	}

	mac = lnk.Attrs().HardwareAddr.String()

	// DHCP
	results := dhclient.SendRequests(
		context.TODO(), []netlink.Link{lnk}, true, false, dhclient.Config{
			Timeout:  15 * time.Second,
			Retries:  3,
			LogLevel: dhclient.LogDebug,
			V4ServerAddr: &net.UDPAddr{
				IP:   net.IPv4bcast,
				Port: dhcpv4.ServerPort,
			},
		},
		30*time.Second,
	)

	for result := range results {
		if result.Err != nil {
			log.Printf("lease error: %v", result.Err)
			continue
		}
		err := result.Lease.Configure()
		if err != nil {
			return fmt.Errorf("lease configure: %v", err)
		}
		return nil
	}

	return fmt.Errorf("dhcp failed")

}

func runsled() error {

	// connect to sled apiserver
	client, conn, err := sledClient()
	if err != nil {
		return err
	}
	defer conn.Close()

	// wait for events

	stream, err := client.Wait(context.Background())
	if err != nil {
		return err
	}

	err = stream.Send(&facility.SledWaitRequest{Mac: mac, Ack: nil})
	if err != nil {
		return err
	}
	log.Printf("sent initial request: %s", mac)

	log.Printf("entering event loop")
	for {

		resp, err := stream.Recv()
		if err == io.EOF {
			log.Printf("got EOF from sledapi")
			break
		}
		if err != nil {
			return fmt.Errorf("stream recv: %v", err)
		}

		switch r := resp.WaitResponse.(type) {
		case *facility.SledWaitResponse_Standby:
			log.Printf("got standby request")
			ack(stream, facility.SledState_Waiting, retries)
		case *facility.SledWaitResponse_Stamp:
			log.Printf("[stamp] %+v", r.Stamp)

			// prevent duplicated work
			if image == r.Stamp.Image {
				log.Printf("same image, nothing to do")
				ack(stream, facility.SledState_Stamp, retries)
			} else {
				if benchmark {
					benchImages(r.Stamp.Device, r.Stamp.Server)
					os.Exit(0)
				}

				stampImage(r.Stamp.Image, r.Stamp.Device, r.Stamp.Server)
				if err != nil {
					log.Printf("stamp image error: %v", err)
					nack(stream, err, retries)

					// if we get an error stamping, this is going to force the client
					// to reset the loop to try forever until the server tells the client
					// to do something different.
					return err
				} else {
					ack(stream, facility.SledState_Stamp, retries)
				}
			}

		case *facility.SledWaitResponse_Kexec:
			log.Printf("[kexec] %+v", r.Kexec)
			err := kexecImagePrepare(
				r.Kexec.Kernel,
				r.Kexec.Initramfs,
				r.Kexec.Server,
				r.Kexec.Cmdline,
			)
			if err != nil {
				log.Printf("kexec prepare error: %v", err)
				nack(stream, err, retries)

				// same as stamp, if there is an error, just retry ad nauseam
				return err
			} else {
				ack(stream, facility.SledState_Kexec, retries)

				err := kexecImageReboot()

				if err != nil {
					log.Printf("kexec prepare error: %v", err)
					nack(stream, err, retries)

					// same as stamp, if there is an error, just retry ad nauseam
					return err
				}
			}
		}

	}

	return nil

}

func nack(stream facility.Sled_WaitClient, cerr error, retry int) {
	if retry < 0 {
		log.Printf("failed to nack- leaving")
		return
	}

	toSend := &facility.SledWaitRequest{
		Ack: &facility.SledAck{
			Ack:   &facility.SledAck_None{true},
			Error: fmt.Sprintf("%v", cerr),
		},
		Mac: mac,
	}

	err := stream.Send(toSend)
	if err != nil {
		log.Printf("failed to send: %+v, %v", toSend, err)
		time.Sleep(2 * time.Second)

		// recurse
		nack(stream, cerr, retry-1)
	}

	log.Printf("negative acknowledgement sent: %+v", toSend)
}

func ack(stream facility.Sled_WaitClient, state facility.SledState_State, retry int) {
	if retry < 0 {
		log.Printf("failed to ack- leaving")
		return
	}

	resp := &facility.SledAck{}

	// we will set the image to none, so the response as a non-nil
	// string value for server to recognize
	if image == "" {
		image = "none"
	}

	switch state {
	case facility.SledState_Stamp:
		resp.Ack = &facility.SledAck_Stamp{true}
	case facility.SledState_Waiting:
		resp.Ack = &facility.SledAck_Standby{image}
	case facility.SledState_Kexec:
		resp.Ack = &facility.SledAck_Kexec{true}
	default:
		log.Printf("unknown state to ack: %+v", state)
		return
	}

	toSend := &facility.SledWaitRequest{
		Ack: resp,
		Mac: mac,
	}

	err := stream.Send(toSend)
	if err != nil {
		log.Printf("failed to send: %+v, %v", toSend, err)
		time.Sleep(2 * time.Second)

		// recurse
		ack(stream, state, retry-1)
	}

	log.Printf("acknowledgement sent: %+v", toSend)

	return
}

func stampImage(name, dest, server string) error {
	return stampImageDecompressor(name, dest, server, nil)
}

func stampImageDecompressor(name, dest, server string, decompr *decompression.Decompressor) error {

	mc, err := marsMinIOImageClient(server)
	if err != nil {
		return fmt.Errorf("minio connect: %v", err)
	}

	obj, err := mc.GetObject(context.TODO(), "images", name, minio.GetObjectOptions{})
	if err != nil {
		return fmt.Errorf("minio get object: %v", err)
	}
	defer obj.Close()

	out, err := os.OpenFile(dest, os.O_WRONLY, 0600)
	if err != nil {
		return fmt.Errorf("open %s: %v", dest, err)
	}
	defer out.Close()

	log.Printf("begin image copy")

	if decompr != nil {
		err = (*decompr)(out, obj)
	} else {
		err = decompression.MagicDecompressorBySeekable(out, obj)
	}

	if err != nil {
		return fmt.Errorf("decompress: %v", err)
	}

	image = name
	log.Printf("image copy complete")

	return nil
}

func kexecImagePrepare(kernel, initramfs, server, cmdline string) error {

	mc, err := marsMinIOImageClient(server)
	if err != nil {
		return nil
	}

	debug := true
	kpath := "/tmp/kernel"
	ipath := "/tmp/initramfs"

	err = writeTemp(mc, server, kernel, kpath)
	if err != nil {
		return fmt.Errorf("writeTemp %v", err)
	}

	err = writeTemp(mc, server, initramfs, ipath)
	if err != nil {
		return fmt.Errorf("writeTemp %v", err)
	}

	img := &boot.LinuxImage{
		Kernel:  uio.NewLazyFile(kpath),
		Initrd:  uio.NewLazyFile(ipath),
		Cmdline: cmdline,
	}
	err = img.Load(debug)
	if err != nil {
		return fmt.Errorf("kexec load: %v", err)
	}

	return nil

}

func kexecImageReboot() error {
	err := kexec.Reboot()
	if err != nil {
		return fmt.Errorf("kexec reboot: %v", err)
	}

	return nil
}

func writeTemp(mc *minio.Client, server, source, dest string) error {

	obj, err := mc.GetObject(context.TODO(), "images", source, minio.GetObjectOptions{})
	if err != nil {
		return fmt.Errorf("minio get object: %v", err)
	}

	out, err := os.Create(dest)
	if err != nil {
		return fmt.Errorf("open %s: %v", dest, err)
	}
	defer out.Close()

	log.Printf("begin file copy")

	_, err = io.Copy(out, obj)
	if err != nil {
		return fmt.Errorf("copy: %v", err)
	}

	log.Printf("file copy complete")

	return nil

}
